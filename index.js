module.exports.checkStatus = function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    var error = new Error(response.statusText);
    error.response = response;
    throw error;
};

module.exports.parseJSON = function parseJSON(response) {
    return response.json();
};
